Xero API Wrapper
======

An API proxy which exposes limited access to a private Xero application:

* Limited to read only access (forwards only get requests to Xero API)
* Limited to agreed accounting modules (no payrolls, no bank statements, etc...)

## How to set up:

### 1) Create certificate files
Xero API access is done by creating a Private app under an authorised Xero account.
A prerequisite for that is to generate a certification files pair to use when creating the app, and subsequently when issuing API requests.
Create .pem and .cer files with the following names:

```
openssl genrsa -out xeroprivatekey.pem 1024
openssl req -new -x509 -key xeroprivatekey.pem -out xeropublickey.cer -days 1825
```

Place the generated **xeroprivatekey.pem** file in the auth directory of the project (you can remove the placeholder file that's there)


### 2) Create a Private Application in Xero
Login to Xero with a user that has access to the organisation data. Follow [these steps](https://api.xero.com) to create a new private application.
When your application is ready you'll be provided with two keys - you can ignore the Consumer Secret, but you'll have to keep the **Consumer Key** - it will be used to authenticate against the certificate file with every API call.

Important: The **Consumer Key** is sensitive. Anyone who wishes to have access to your Xero data via this API wrapper will have to provide it in their request's header **XERO_CONSUMER_KEY**


### 3) Install Python 3.6
Install python 3.6 environment. Consider Miniconda as the environment manager - see details here:
https://medium.freecodecamp.org/why-you-need-python-environments-and-how-to-manage-them-with-conda-85f155f4353c


### 4) Deploy the project
Install necessary python packages by running the following line:

```
pip install -r requirements.txt
```

Place your app's **xeroprivatekey.pem** file in the **auth** folder.


### 5) Setup your server base route and port
Edit the run.py file to set the desired base route and port. The default option is:
<server name or IP>:5001/<endpoints, e.g. Invoicing>

```
from flask import Flask

def create_app(config_filename):
    app = Flask(__name__)
    app.config.from_object(config_filename)

    from app import api_bp
    # app.register_blueprint(api_bp, url_prefix='/api')
    app.register_blueprint(api_bp
                           # , url_prefix='/api' # <== uncomment this line to specify a base route
                           )

    return app

if __name__ == "__main__":
    app = create_app("config")
    app.run(ssl_context = 'adhoc',
            host = None,
            port = 5001, # <== set your desired port
            debug = True)
```


### 6) Configure HTTPS
By default the SSL method is adhoc. [This example](https://stackoverflow.com/questions/29458548/can-you-add-https-functionality-to-a-python-flask-web-server/42906465) shows how to provide proper certificates.


### 7) Run the server
Use the command line to run the `run.py` file:

```
python run.py
```
