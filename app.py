from flask import Blueprint
from flask_restful import Api
from Xero import XeroAPIWrapper

api_bp = Blueprint('api', __name__)
api = Api(api_bp)

# Route
api.add_resource(XeroAPIWrapper, '/<module>',
                                 '/<module>/',
                                 '/<module>/<resourceId>',
                                 '/<module>/<resourceId>/',
                                 '/<module>/<resourceId>/<subfunction>',
                                 '/<module>/<resourceId>/<subfunction>/')
