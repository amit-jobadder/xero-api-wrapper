from flask_restful import Resource, request
from flask_api import status
import json
import requests
from config import *

##########################################
# XeroEndPoint - base class
##########################################
class XeroAPIWrapper(Resource):
    def get(self, module, resourceId=None, subfunction=None):

        data = {}
        responeStatus = None

        try:
            
            ######################
            # module
            ######################
            if module not in ['Invoices',
                              'Payments',
                              'Contacts',
                              'ContactGroups',
                              'CreditNotes',
                              'Currencies',
                              'RepeatingInvoices']:

                data = {"error": f'Unsupported module {module}'}
                responeStatus = status.HTTP_400_BAD_REQUEST
                return data, responeStatus


            ######################
            # resourceId
            ######################
            if resourceId != None:
                resourceId = f'/{resourceId}'
            else:
                resourceId = ''


            ######################
            # subfunction
            ######################
            if subfunction != None:
                subfunction = f'/{subfunction}'
            else:
                subfunction = ''


            ######################
            # extract query params
            ######################
            try:
                params = ''
                if len(request.url.split("?")) > 0:
                    params = f'?{request.url.split("?")[1]}'
            except:
                pass


            ######################
            # consumer key
            ######################
            try:
                consumer_key = request.headers['XERO_CONSUMER_KEY']
            except:
                data = {"message": "CONSUMER_KEY header was not provided"}
                responeStatus = status.HTTP_400_BAD_REQUEST
                return data, responeStatus


            ######################
            # authenticate
            ######################
            from oauthlib.oauth1 import (SIGNATURE_RSA, SIGNATURE_TYPE_AUTH_HEADER)
            from requests_oauthlib import OAuth1

            with open(XeroCertificatePath) as keyfile:
            	rsa_key = keyfile.read()

            oauth = OAuth1(client_key = consumer_key,
            			   resource_owner_key = consumer_key,
            			   rsa_key = rsa_key,
            			   signature_method = SIGNATURE_RSA, # "RSA-SHA1"
            			   signature_type = SIGNATURE_TYPE_AUTH_HEADER # "AUTH_HEADER"
            			   )


            ######################
            # Call Xero api
            ######################
            url = f'https://api.xero.com/api.xro/2.0/{module}{resourceId}{subfunction}{params}'
            print(f'Calling {url}')
            response = requests.get(url = url,
                                    headers = {"accept": "application/json"},
                                    auth = oauth)

            data = {"message": response.reason}
            responeStatus = response.status_code

            if response.ok == True:
                try:
                    # For responses resulting in status 2xx - forward response data
                    if str(response.status_code)[0] == '2':
                        data = response.json()
                except:
                    pass
        except Exception as e:
            data = {"message": f'Internal exception - {str(e)}'}
            responeStatus = status.HTTP_400_BAD_REQUEST

        return data, responeStatus
