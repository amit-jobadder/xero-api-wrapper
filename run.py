from flask import Flask

def create_app(config_filename):
    app = Flask(__name__)
    app.config.from_object(config_filename)

    from app import api_bp
    # app.register_blueprint(api_bp, url_prefix='/api')
    app.register_blueprint(api_bp
                           # , url_prefix='/api' # <== uncomment this line to specify a base route
                           )

    return app


if __name__ == "__main__":
    app = create_app("config")
    app.run(
            #ssl_context = 'adhoc', # <== comment this line to avoid https
            host = None,
            port = 5001, # <== set your desired port
            debug = True)
